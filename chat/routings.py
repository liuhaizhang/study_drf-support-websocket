from django.urls import path
from . import consumer

# 这个变量是存放websocket的路由
socket_urlpatterns = [
    path('chat/socket/', consumer.ChatView.as_asgi()),
    path('chat/<str:name>/',consumer.AllChatView.as_asgi()),
    path('chat/group/<str:name>/',consumer.AllChatChannelsLayerView.as_asgi())
]