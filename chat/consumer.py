from channels.generic.websocket import WebsocketConsumer
from channels.exceptions import StopConsumer
from asgiref.sync import async_to_sync
import time

ALL_USER_LIST =[]

class ChatView(WebsocketConsumer):
    def websocket_connect(self, message):
        # 客户端与服务端进行握手时，会触发这个方法
        # 服务端允许客户端进行连接，就是握手成功
        self.accept()

    def websocket_receive(self, message):
        # 接收到客户端发送的数据
        recv = message.get('text')
        print('接收到的数据，', recv)
        if recv == 'close':
            # 服务的主动断开连接
            print('服务器断开连接')
            self.close()
        else:
            # 客户端向服务端发送数据
            self.send(f'我收到了，{time.strftime("%Y-%m-%d %H:%M:%S")}')

    def websocket_disconnect(self, message):
        # 客户端端口连接时，会触发该方法，断开连接
        print('客户端断开连接')
        raise StopConsumer()

class AllChatView(WebsocketConsumer):
    def websocket_connect(self, message):
        self.accept()
        ALL_USER_LIST.append(self)
        #拿到地址栏中的有名分组
        name = self.scope.get('url_route').get('kwargs').get('name')
        for user in ALL_USER_LIST:
            if user == self:
                continue
            user.send(f'{name},进入群聊了')

    def websocket_receive(self, message):
        text = message.get('text')
        name = self.scope.get('url_route').get('kwargs').get('name')
        for user in ALL_USER_LIST:
            if user == self:
                continue
            user.send(f'{name}: {text}')

    def websocket_disconnect(self, message):
        name = self.scope.get('url_route').get('kwargs').get('name')
        for user in ALL_USER_LIST:
            if user == self:
                continue
            user.send(f'{name}:退出群聊了！！')
        raise StopConsumer()

class AllChatChannelsLayerView(WebsocketConsumer):
    def websocket_connect(self, message):
        #接收客户端的连接
        self.accept()
        #将客户端连接对象放到1004组内（组名可以随意）
        #self.channel_layer.group_add 方法是异步的，要转成同步的
        async_to_sync(self.channel_layer.group_add)('1004',self.channel_name)
        #拿到地址栏中的有名分组
        name = self.scope.get('url_route').get('kwargs').get('name')
        #群发,通知组内的所有客户端，去执行xx_oo方法，在xx_oo 方法中可以定义任意的方法
        async_to_sync(self.channel_layer.group_send)('1004',{"type":"xx.oo",'message':f'{name},进入群聊'})

    def websocket_receive(self, message):
        text = message.get('text')
        name = self.scope.get('url_route').get('kwargs').get('name')
        # 群发
        async_to_sync(self.channel_layer.group_send)('1004', {"type": "xx.oo", 'message': f'{name}：{text}'})

    def websocket_disconnect(self, message):
        name = self.scope.get('url_route').get('kwargs').get('name')
        async_to_sync(self.channel_layer.group_send)('1004', {"type": "xx.oo", 'message': f'{name},推出群聊'})
        #将这个从群内移除，组名，自己的名字
        async_to_sync(self.channel_layer.group_discard)('1004',self.channel_name)
        raise StopConsumer()

    def xx_oo(self,event):
        # print(event)
        #event，
        text = event.get('message')
        #这里的send是给当前组内所有客户端发送消息的
        self.send(text)