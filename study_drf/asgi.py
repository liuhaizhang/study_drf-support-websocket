"""
ASGI config for study_drf project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os
from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
# 导入chat应用中的路由模块
from chat import routings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'study_drf.settings')
application = ProtocolTypeRouter({
    # http路由走这里
    "http": get_asgi_application(),
    # chat应用下rountings模块下的路由变量socket_urlpatterns
    "websocket": URLRouter(routings.socket_urlpatterns)
})
